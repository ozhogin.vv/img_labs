export default function saveImage(image) {
    let canvasHelper = document.createElement('canvas');
    canvasHelper.width = image.width;
    canvasHelper.height = image.height;
        
    let canvasHelperCtx = canvasHelper.getContext('2d');
    canvasHelperCtx.putImageData(image, 0, 0);

    canvasHelper.toBlob(async function (blob) {
        const supportsFileSystemAccess =
        'showSaveFilePicker' in window &&
        (() => {
            try {
                return window.self === window.top;
            } catch {
                return false;
            }
        })();
        if (supportsFileSystemAccess) {
            try {
                const opts = {
                    suggestedName: 'ozhogin_image.jpg',
                    types: [
                        {
                            description: "JPEG",
                            accept: { "image/jpeg": [".jpg", ".jpeg"] },
                        },
                        {
                            description: "PNG",
                            accept: { "image/png": [".png"] },
                        },
                        {
                            description: "GIF",
                            accept: { "image/gif": [".png"] },
                        },
                        {
                            description: "TIFF",
                            accept: { "image/tiff": [".tif", ".tiff"] },
                        },
                        {
                            description: "WEBP",
                            accept: { "image/webp": [".webp"] },
                        },
                    ],
                };
                const handle = await showSaveFilePicker(opts);
                const writable = await handle.createWritable();
                await writable.write(blob);
                await writable.close();
            } catch (err) {
                if (err.name !== 'AbortError') {
                    console.error(err.name, err.message);
                }
            }
        } else {
            let blobURL = URL.createObjectURL(blob);
            let link = document.createElement("a");
            link.href = blobURL;
            link.download = "ozhogin_image.jpg";
            link.click();
            setTimeout(() => {
                link.remove();
            }, 1000);  
        }
    })
}    
