let colorCorrectionInputs = document.getElementsByClassName('color_correction_input');
let chartData = [];

export default function drawColorCorrectionLine(chart, isDefault) {
    if (!chart) {
        return chartData.map((el) => {return el[1]});
    }

    if (chart.getSeriesCount() > 3) {
        chart.removeSeriesAt(chart.getSeriesCount()-1);
    }
   
    let firstIn = parseInt(colorCorrectionInputs[0].value);
    let firstOut = parseInt(colorCorrectionInputs[1].value);
    let secondIn = parseInt(colorCorrectionInputs[2].value);
    let secondOut = parseInt(colorCorrectionInputs[3].value);

    if (isDefault) {
        firstIn = 0;
        firstOut = 0;
        secondIn = 255;
        secondOut = 255;
    }

    function calculateLinePoint(x1, y1, x2, y2, x) {
        let k = (y2 - y1) / (x2 - x1);
        let b = y1 - k * x1;
        return k * x + b;
    }

    for (let x = 0; x < firstIn; x++) {
        chartData[x] = [x, firstOut];
    }

    for (let x = firstIn; x < secondIn; x++) {
        let y = calculateLinePoint(firstIn, firstOut, secondIn, secondOut, x);
        chartData[x] = [x, y];
    }

    for (let x = secondIn; x < 256; x++) {
        chartData[x] = [x, secondOut];
    }
    
    let dataSet = anychart.data.set(chartData);
    
    let seriesData = dataSet.mapAs({'x': 0, 'value': 1});

    let series = chart.line(seriesData);
    series.stroke('black', 2);
    series.hovered().markers().enabled(false);

    chart.xAxis().labels().format(function() {
        return this.value == 0 || this.value == 255 || this.value == firstIn || this.value == secondIn ? this.value : '';
    });

    chart.container('chart_container').draw();
}
