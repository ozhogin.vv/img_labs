import drawColorCorrectionLine from "./drawColorCorrectionLine";
import checkColorCoords from "./checkColorCoords";
import applyColorCorrection from "./applyColorCorrection";

let colorCorrectionOffcanvas = document.getElementById('color_correction');

let colorButton = document.getElementById("color_button");
let submitColorCorrectionButton = document.getElementById("submit_color_correction_button");
let resetColorCorrectionButton = document.getElementById("reset_color_correction_button");
let closeColorpanelButton = document.getElementById("close_color_correction_button");
let colorCorrectionInputs = document.getElementsByClassName('color_correction_input');
let previewCheckbox = document.getElementById("color_correction_checkbox");

export default function drawChart(imageData) {
  let chartContainer = document.getElementById('chart_container');
  chartContainer.innerHTML = ''; 

  let R = Array(256).fill(0);
  let G = Array(256).fill(0);
  let B = Array(256).fill(0);

  for (let i = 0; i < imageData.length; i+=4) {
    R[imageData[i]] += 1;  
    G[imageData[i + 1]] += 1;  
    B[imageData[i + 2]] += 1;  
  }

  let maxValRatio = Math.max(...R, ...G, ...B) / 256;
  let chartData = [];

  for (let i = 0; i < 256; i++) {
    chartData[i] = [i, R[i] / maxValRatio, G[i] / maxValRatio, B[i] / maxValRatio];
  }

  let chart = anychart.line();

  chart.xAxis().labels().format(function() {
      return this.value == 0 || this.value == 255 ? this.value : '';
  });
  
  chart.xAxis().ticks().stroke('none');
  chart.xAxis().title('in');
  chart.yAxis().title('out');
  chart.yScale().ticks().set([0, 255]);
  chart.yScale().maximum(255);
  
  chart.tooltip().enabled(false);

  
  let dataSet = anychart.data.set(chartData);

  let seriesData_1 = dataSet.mapAs({'x': 0, 'value': 1});
  let seriesData_2 = dataSet.mapAs({'x': 0, 'value': 2});
  let seriesData_3 = dataSet.mapAs({'x': 0, 'value': 3});

  let seriesConfiguration = function (series, name) {
    series.name(name);
    series.hovered().markers()
      .enabled(false)
  };

  let series;

  series = chart.line(seriesData_1);
  series.stroke('red');
  seriesConfiguration(series, 'R');

  series = chart.line(seriesData_2);
  series.stroke('green');
  seriesConfiguration(series, 'G');

  series = chart.line(seriesData_3);
  series.stroke('blue');
  seriesConfiguration(series, 'B');

  chart.container(chartContainer).draw();

  drawColorCorrectionLine(chart);

  closeColorpanelButton.onclick = () => {
    colorButton.classList.remove('active');
    applyColorCorrection(false);
    previewCheckbox.checked = false;
  }


  resetColorCorrectionButton.onclick = () => {
    applyColorCorrection(false);
    drawColorCorrectionLine(chart, true);
  }

  submitColorCorrectionButton.onclick = () => {
    let isColorCoordsCorrect = checkColorCoords();
    
    if (isColorCoordsCorrect) {
      let colorCorrectionOffcanvasInstance = bootstrap.Offcanvas.getInstance(colorCorrectionOffcanvas);

      if (colorCorrectionOffcanvasInstance) {
        colorCorrectionOffcanvasInstance.hide();
        colorButton.classList.remove('active');
      }

      applyColorCorrection();
      drawColorCorrectionLine(chart, true);
    }
  }

  previewCheckbox.onclick = () => {
    applyColorCorrection(previewCheckbox.checked);
  } 

  for (let i = 0; i < 4; i++) {
    colorCorrectionInputs[i].onchange = () => {
      if (checkColorCoords()) {
        drawColorCorrectionLine(chart);
        if (previewCheckbox.checked) {
          applyColorCorrection(previewCheckbox.checked);
        }
      }
    }
  }
}
