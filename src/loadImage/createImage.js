export default async function createImage({file, url}) {
	let image = new Image();
	
	if (!file) {
		let response = await fetch(url);
		let blob = await response.blob();
		image.src = URL.createObjectURL(blob);
	} else {
		image.src = URL.createObjectURL(file);
	}

	return image;
}
