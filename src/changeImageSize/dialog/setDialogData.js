import drawScaledImage from "../../drawScaledImage/drawScaledImage";
import checkScale from "./checkScale";
import setMpAfterChange from "./setMpAfterChange";
import writeUnitsOnChange from "./writeUnitsOnChange";
import changeRatioSideInput from "./changeRatioSideInput";


let dialogButton = document.getElementById("dialog_button");
let closeDialogButton = document.getElementById("close_dialog_button");
let submitDialogButton = document.getElementById("submit_dialog_button");

let units = document.getElementsByClassName('dialog_units');
let mpBeforeChange = document.getElementById("dialog_before_px"); 
let mpAfterChange = document.getElementById("dialog_after_px"); 
let radioPercent = document.getElementById('radio_percent');
let radioPx = document.getElementById('radio_px');
let inputWidth = document.getElementById('input_width');
let inputHeight = document.getElementById('input_height');
let ratioCheck = document.getElementsByClassName('ratio_check__input')[0];


export default function setDialogData(image) {
    dialogButton.onclick = () => { 
        window.dialog.showModal();

        inputHeight.value = image.height;
        inputWidth.value = image.width;

        mpBeforeChange.innerText = "До изменений: " + (image.width *  image.height / 1000000).toFixed(2) + " MP";
        mpAfterChange.innerText = "После изменений: " + (image.width *  image.height / 1000000).toFixed(2) + " MP";

        radioPx.checked = true;

        units[0].innerText = radioPx.value;
        units[1].innerText = radioPx.value;
    
        writeUnitsOnChange(radioPercent, image.width, image.height);
        writeUnitsOnChange(radioPx, image.width, image.height);
    }

    closeDialogButton.onclick = () => { window.dialog.close() };

    ratioCheck.addEventListener('change', function() {
        if (ratioCheck.checked) {
            changeRatioSideInput(inputWidth, inputHeight, image.height / image.width);
        }
    })

    inputWidth.addEventListener("input", function() {
        if (ratioCheck.checked) {
            changeRatioSideInput(inputWidth, inputHeight, image.height / image.width);
        }
        setMpAfterChange(image.width, image.height);
        checkScale(inputWidth, image.width);
    })

    inputHeight.addEventListener("input", function() {
        if (ratioCheck.checked) {
            changeRatioSideInput(inputHeight, inputWidth,  image.width / image.height);
        }
        setMpAfterChange(image.width, image.height);
        checkScale(inputHeight, image.height);
    })

    submitDialogButton.onclick = () => {
        let widthScale = checkScale(inputWidth, image.width);
        let heightScale = checkScale(inputHeight, image.height);
        
        if (widthScale && heightScale) {
            drawScaledImage(image, widthScale, heightScale);
            window.dialog.close();
        }
    }
}
