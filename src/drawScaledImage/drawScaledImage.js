import changeImageSize from '../changeImageSize/changeImageSize';
import renderImageData from './renderImageData';
import setDialogData from '../changeImageSize/dialog/setDialogData';
import saveImage from "../saveImage";
import getImageMargins from './getImageMargins';


let con = document.getElementsByClassName("con")[0];
let conWidth = con.offsetWidth;
let conHeight = con.offsetHeight;
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext('2d');

let scaleRange = document.getElementById("scale_range");
let scaleNum = document.getElementById("scale_num");

let saveImageButton = document.getElementById("save_button");

export default function drawScaledImage(image, widthScale, heightScale) {
	let firstLoad = false;

	if (!widthScale) {
		let scale = Math.min((conWidth - 100) / image.width, (conHeight - 100) / image.height);
		widthScale = scale;
		heightScale = scale;
		firstLoad = true;
	}
	
	let margins = getImageMargins(conWidth, conHeight, image.width * widthScale, image.height * heightScale, firstLoad);

	canvas.style.marginLeft = margins.left + 'px';
	canvas.style.marginTop = margins.top + 'px';
	canvas.style.position = 'static';

	canvas.width = image.width * widthScale;
	canvas.height = image.height * heightScale;

	scaleRange.value = (widthScale * 100).toFixed(2);
	scaleNum.value = (widthScale * 100).toFixed(2);

	let scaledImage = changeImageSize(image, widthScale, heightScale);

	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.putImageData(scaledImage, 0, 0);
		
	saveImageButton.onclick = () => { saveImage(scaledImage) } ;

	renderImageData(image, canvas, ctx);
	setDialogData(image);
}
