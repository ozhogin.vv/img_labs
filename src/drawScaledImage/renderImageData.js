import drawScaledImage from "./drawScaledImage";

let imageSize = document.getElementById("image_size");

let pixelCoordinates = document.getElementById("pixel_coordinates");
let pixelColorBlock = document.getElementById("pixel_color_block");
let pixelColor = document.getElementById("pixel_color");

let scaleLabel = document.getElementById("scale_label");
let scaleRange = document.getElementById("scale_range");
let scaleNum = document.getElementById("scale_num");
let scalePercent = document.getElementById("scale_percent");

export default function renderImageData(image, canvas, ctx) {

	imageSize.innerText = 'Размер: ' + image.width + ' * ' + image.height;

	canvas.addEventListener('mousedown', e => {
		let [r, g, b] = ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
		pixelCoordinates.innerText = `x: ${e.offsetX} y: ${e.offsetY} `;
		pixelColor.innerText = `rgb(${r}, ${g}, ${b})`;
		pixelColorBlock.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
		pixelColorBlock.style.display = "inline-block";
	});

	scaleLabel.style.display = "inline-block";
	scaleRange.style.display = "inline-block";
	scaleNum.style.display = "inline-block";
	scalePercent.style.display = "inline-block";
	
	scaleRange.addEventListener("change", function() {
		drawScaledImage(image, this.value / 100, this.value / 100);
	});

	scaleNum.addEventListener("change", function() {
		let val = parseInt(this.value);
		if (val < 12) this.value = 12;
		if (val > 300) this.value = 300;

		drawScaledImage(image, this.value / 100, this.value / 100);
	});
}
